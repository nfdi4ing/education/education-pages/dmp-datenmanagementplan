## 6. Beispielhafte Inhalte: Was ist Teil des DMP?

**Beispielhafte Inhalte i/ii**    
*Projektbezogene Angaben*

* Standarddaten:
    -   Projektname sowie Projektstart und –ende
    -   Verantwortliche
    -   Förderprogramm (optional)
    -   Etc.

* Overhead:
    -   Datenschutz und rechtliche Aspekte
    -   Kosten
    -   Projektplanung
    -   etc. 

* Archivierung und Nachnutzung:
    -   Wo und wie werden die Daten nach dem Projekt gespeichert?
    -   Welche Daten sollen wie geteilt werden? (optional)
    -   Wie werden (alle) Daten gesichert?


**Beispielhafte Inhalte ii/ii**    
*Angaben zu erhobenen Daten*

* Angaben zu Forschungsdaten:
    -   Welche Daten werden erwartet?
    -   Welchen Umfang werden die Daten haben? (kB, MB, GB, TB,…?)
    -   Wie werden die Daten produziert / verarbeitet / ausgewertet / archiviert?
    -   Wie heißen (bei relationalen Daten) die Attribute? 
    -   Welche (SI-)Einheit haben die Werte? 
    -   Wurden die Daten validiert? (I. S. v. Plausibilitätscheck, Regelbasierter Prüfung etc.) 
    -   Handelt es sich um gemessene Daten oder um berechnete Ergebnisse?

    
* Metadaten:
    -   Werden Metadaten erfasst? Wenn ja welche?
    -   Wie und wo werden Metadaten erfasst?
