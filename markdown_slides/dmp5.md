## 5. Nutzen: welche Vorteile bietet ein DMP?

**Nutzen**    
*Warum sollte ein DMP genutzt werden?*

* Steigerung der Qualität und Effizienz der Forschungsarbeit
* Verhinderung von Doppelarbeit durch bessere Organisation
* Es existieren bereits Vorlagen für DMPs, sodass Forschende sich wenig Gedanken über die Gestaltung machen müssen
* Einfache Nutzung von bereits erarbeiteten Ergebnissen 
* Anforderungen von Fördergebern und gute wissenschaftliche Praxis 
    -   hierbei oft unklar, was genau gefordert wird 
    -   Tools wie RDMO beinhalten Fragenkataloge extra für spezielle Fördergeber
*besser organisierter Overhead + Übung im Umgang mit DMP	= mehr Zeit für Anderes* 


**Nutzen**    
*Welcher ingenieurspeziefische Mehrwert besteht?*

* Einfache Übersicht über das Projekt samt Verantwortlichkeiten, genutzten Speicherorten und zugrundeliegenden Daten 
+ Wiederauffindbarkeit von Daten des momentanen und früherer Projekte wird gewährleistet
+ Planung der Handhabung von großen Datenmengen, bspw. Sensordaten aus Experimenten 
+ Verfügbarmachung und Nachnutzung von CAx (z.B. CAD) Daten planen 
+ Evaluation zur Handhabung von bspw. Produktdaten aus dem Feld
+ Definition von wichtigen Rahmenbedingungen, sodass Unklarheiten von Beginn an beseitigt werden