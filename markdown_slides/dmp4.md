## 4. Zweck: Was kann ein DMP

**Zweck**    
*Wofür können DMP genutzt werden?*

* Erfüllung von Anforderungen der Fördergeber (derzeit noch, später auch Folgendes)
* Kommunikationstool zwischen Projektpartnern -> Single Point of Information
* Selbstmanagement/projektinternes FDM:
    -   Welche Daten werden erwartet?
    -   Welche fallen tatsächlich an?
    -   Welche fehlen noch?
* Vorbereitung der Nach- und Weiternutzung:
    -   Bei Projektabschluss bleiben Daten zugänglich und verständlich
    -   Nachnutzbarkeit wird gewährleistet
    -   Für Andere (z.B. Kollegen/Vorgesetzte) sind die Daten leichter zugänglich -> Weniger Arbeit beim Einarbeiten Anderer in das Projekt

<br>
<br>
<br>
<br>

[TU9-FDM. (2020, August 6). Datenmanagementplan. Zenodo. https://doi.org/10.5281/zenodo.3974562 ]