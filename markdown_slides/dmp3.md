## 3. Motivation: Warum ein DMP?

**Motivation**

* Wenn Forschende damit konfrontiert werden, einen Datenmanagementplan schreiben zu müssen, fehlt zum Einen oft die Erfahrung und zum Anderen wird das Erstellen eines Datenmanagementplans als zu aufwendig betrachtet
* Aus Erfahrung nimmt das Erstellen eines DMPs jedoch nicht viel Zeit in Anspruch, da es bereits einige Tools gibt, die das Anfertigen eines DMPs deutlich erleichtern
* Beispiele für solche Tools sind:
    * [RDMO](https://rdmo.nfdi4ing.de/) 
    * [DMP-Tool des MIT](https://libraries.mit.edu/data-management/plan/write/)

* Darüber hinaus dienen DMPs als Bestandsaufnahme der Projektdaten, sodass unnötige Duplikate vermieden werden
* Die Daten sind gut beschrieben und dokumentiert und daher leicht verständlich
* Ein DMP bietet Kontinuität, wenn bestehende Projektmitglieder ausscheiden oder hinzukommen
* DMPs helfen, Fragen des Urheberrechts bereits in einem frühen Stadium zu berücksichtigen 
* Unterstützt bei der Bewertung von Optionen für die gemeinsame Nutzung von Daten und bei der Planung von Speichervorschriften

<br>

[https://www.st-andrews.ac.uk/research/support/open-research/research-data-management/planning/#d.en.135197]


**Gemeinsame Nutzung von RDMO** 

*Gemeinsames Arbeiten an Datenmanagementplänen*

RDMO: "Innerhalb von RDMO gibt es drei verschiedene Rollen: Nutzer:innen, Manager:innen und Admins.
Als Nutzende:r haben Sie folgende Berechtigungen und Funktionalitäten:

* Erstellung von DMPs und **kollaborative Arbeit** daran mit Ihren Projektpartnern
* Anpassung der Ausgabe mithilfe der Ansichten
* Zentrale Verwaltung von Informationen zum Forschungsdatenmanagement
* fortlaufende Aktualisierbarkeit und Versionierung von Informationen im Verlauf eines Projekts
* Einfrieren des Informationsstandes für Projekte zu bestimmten Zeitpunkten mittels Snapschüsse
* Übersicht über alle eigenen Projekte"

<br>
<br>
<br>
<br>

https://rdmorganiser.github.io/Doku_User/ 