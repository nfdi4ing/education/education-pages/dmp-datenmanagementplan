## Inhaltsverzeichnis

1. [Ziele des Trainings](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/2)
2. [Definition: Was ist ein DMP?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/3)
3. [Motivation: Warum ein DMP?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/5)
4. [Zweck: Was kann ein DMP](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/7)
5. [Nutzen: Welche Vorteile bietet ein DMP?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/9)
6. [Beispielhafte Inhalte: Was ist Teil des DMP?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/11)
7. [Use-Cases: Anwendungsbeispiele I](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/13)
8. [Use-Cases: Anwendungsbeispiele II](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/14)
9. [Übung mit dem Tool RDMO](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/15)
10. [Kontakt](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dmp-datenmanagementplan/html_slides/dmp.html#/16)

