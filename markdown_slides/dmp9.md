## 9. Übung mit dem Tool RDMO 

**Datenmanagementplan in RDMO I/IV**

*Themenvorstellung: Mensch-Maschine-Interaktion in Entscheidungsprozessen*

Erstellen Sie anhand des Beispiels für einen der folgenden Fälle einen groben DMP:  
 - Ihr letztes/momentanes Forschungsprojekt 
 - Ihre Abschlussarbeit
 - Ihre Dissertation
 - Fiktives Projekt:      

*Entwicklung einer Gleichung zur Berechnung der Lichtgeschwindigkeit mit der Absicht, die Validierung von einem Kollegen durchführen zu lassen*

Wählen Sie für die Forschungsdaten
 - ein geeignetes Repositorium
 - eine geeignete (Nachnutzungs-)Lizenz 
 - passende Standards für die Metadaten

Versuchen Sie, so viele Informationen wie möglich anzugeben
 - Nutzen Sie die RDMO-Instanz der NFDI4Ing unter https://rdmo.nfdi4ing.de  

*Zu Repositorien, Nachnutzung und Metadaten gibt es ein separates NFDI4Ing Training mit Infos, Übungen und Toolempfehlungen*


**Exkurs: RDMO**

*Research Data Management Organiser (RDMO)*

RDMO ist ein webbasiertes Werkzeug zur Erstellung und Pflege von Datenmanagementplänen. Es ermöglicht, das Datenmanagement in einem Projekt zu planen und zu dokumentieren. Die eingegebenen Informationen können zudem in verschiedenen Formaten ausgegeben werden, wie sie einige Forschungsförderer (z. B. die EU) vorgeben.

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO1.png)


Merkmale von RDMO:
 - Durch die DFG gefördert
 - Beteiligte Partner: Leibniz-Institut für Astrophysik Potsdam (AIP), FH Potsdam, Karlsruher Institut für Technologie
 - Open Source

Weitere Infos finden Sie unter:
 - https://rdmorganiser.github.io/ 
 - https://github.com/rdmorganiser/rdmo 

[https://help.itc.rwth-aachen.de/service/7ab6210773b04ef28a1a8cb33628be67/article/3490afa8cb034e4d9c0fb8b17e6b92cd/,  
https://rdmorganiser.github.io/, 
https://github.com/rdmorganiser/rdmo, 
https://forschungsdaten.org/index.php/RDMO]


**Datenmanagementplan in RDMO II/IV**

*Anleitung zur Nutzung von RDMO I/II*

Gehen Sie wie folgt vor:
 - Loggen Sie sich bei https://rdmo.nfdi4ing.de ein
 - Wählen Sie „Neues Projekt erstellen aus“
![https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO2.png](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO2.png)
 - Geben Sie einen Titel und eine aussagekräftige Beschreibung für Ihr Projekt ein
 - Wählen Sie einen Katalog aus, z.B. „NFDI4Ing_Vorlage“
![https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO3.png](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO3.png)


**Datenmanagementplan in RDMO III/IV**

*Anleitung zur Nutzung von RDMO II/II*

Gehen Sie wie folgt vor (Fortsetzung):
 - Lassen Sie das Feld „Übergeordnetes Projekt“ frei und klicken Sie anschließend auf „neues Projekt erstellen“
![https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO4.png](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO4.png)
 - Klicken Sie auf „Fragen beantworten“
![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO5.png)
 - Füllen Sie den Fragenkatalog aus und klicken Sie jeweils auf „Sichern und fortfahren“
![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO6.png)


**Datenmanagementplan in RDMO IV/IV**

*Anleitung zum Expoert der Übungsergebnisse*

Exportieren Sie die erstellten Daten als .xml aus RDMO.

Diese .xml-Datei können Sie nutzen, um Ihren Arbeitsstand wieder herzustellen, die erstellten Daten zu teilen oder in andere Programme einzulesen.

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/RDMO7.png)

Hinweis: Zum Export bitte mit der rechten Maustaste auf „RDMO XML“ klicken und „Link speichern unter...“ auswählen.
