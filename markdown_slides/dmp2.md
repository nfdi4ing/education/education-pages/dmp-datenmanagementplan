## 1. Ziele des Trainings

**Zu vermittelndes Wissen und Inhalte**

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/dmp_lzm.png)


Quelle: Petersen, Britta, Engelhardt, Claudia, Hörner, Tanja, Jacob, Juliane, Kvetnaya, Tatiana, Mühlichen, Andreas, Schranzhofer, Hermann, Schulz, Sandra, Slowig, Benjamin, Trautwein-Bruns, Ute, Voigt, Anne, & Wiljes, Cord. (2022). Lernzielmatrix zum Themenbereich Forschungsdatenmanagement (FDM) für die Zielgruppen Studierende, PhDs und Data Stewards (Version 1). Zenodo. https://doi.org/10.5281/zenodo.7034478



## 2. Definition: Was ist ein DMP?

**Datenmanagementplan: Definition**    
*Was ist ein DMP? - i/ii*

* Systematische Beschreibung des Umgangs mit (Forschungs)daten während des gesamten Projektverlaufs und darüber hinaus
* Beim Projektbeginn erstellte Sammlung der erwarteten Daten und Zuständigkeiten
* Wird über Projektverlauf aktuell gehalten
* Gewährleistet Nachvollziehbarkeit und Übersichtlichkeit

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Definition1.png)

[Vgl. TU9-FDM. (2020, August 6). Datenmanagementplan. Zenodo. https://doi.org/10.5281/zenodo.3974562]


**Datenmanagementplan: Definition**    
*Was ist ein DMP? - weitere Definitionen ii/ii*

* Angaben zu Projektleitung, Projekttitel, Projektbeschreibung, Förderorganisation, Art und Umfang der entstandenen Forschungsdaten, Speicherung, Aufbewahrungsdauer usw. 
* Ermöglichung der Interpretation, Nachvollziehbarkeit und Nachnutzung von Forschungsergebnissen in späteren Jahren
* Leitfaden für den strukturierten Umgang mit Forschungsdaten während des Projektverlaufs und darüber hinaus
* „[...] Analyse des Workflows von der Erzeugung der Daten bis zu deren Nutzung“

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Definition2.png)

[J. Ludwig, H. Enke (Hrsg.) Leitfaden zum Forschungsdaten-Management. Handreichungen aus dem WissGrid-Projekt. Verlag Werner Hülsbusch: Glückstadt, 2013]

[Darstellung: Vgl. TU9-FDM. (2020, August 6). Datenmanagementplan. Zenodo. https://doi.org/10.5281/zenodo.3974562]