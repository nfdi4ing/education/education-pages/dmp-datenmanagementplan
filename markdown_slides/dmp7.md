## 7. Use-Cases: Anwendungsbeispiele 

**Use-Case 1 - Fluidtechnische Simulation**

*Übersicht über die Arbeitspakete*

AP-1:	 Systematische Literaturrecherche zu bestehenden Berechnungsverfahren    
AP-2:	 Aufstellen von Gleichungen     
AP-3:	 Implementieren der Simulation mit Versionierung		   
AP-4:	 Simulation mit Ergebnissen      
AP-5:	 Aufbau eines Versuchsstands        
AP-6:    Messungen mit aufbereiteten Ergebnissen     
AP-7:    Abgleich der Simulations- und Messergebnisse	      
AP-8:	 Validierung     
AP-9:	 Veröffentlichung der Ergebnisse    

*Info: Sollten die Methoden noch nicht klar sein, so ist es auch möglich, vorerst die Arbeitspakete aufzulisten und später die Methoden zu ergänzen. (siehe auch nächste Folie)*


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase1-3.png)


**Use-Case 1 - Fluidtechnische Simulation**

*Übersicht über die Methoden de Datenerstellung*

AP-4:		    Simulation mit Ergebnissen
 - Input: 	
    - .xlsx mit Randbedingungen für den Differentialgleichungslöser	
    - .xlsx mit geometrischen Abmessungen, ... (weitere Parameter)
 - Tool: 
    - MATLAB, 	
    - Skript aus AP-3
- Output: 	
    - .csv-Daten aus Simulation: Druckwerte mit Positionen (3D-Koordinaten, Spalten 1-3) und Werten an der jeweiligen Position (Spalte 4) in MPa 


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase1-4.png)


**Use-Case 1 - Fluidtechnische Simulation**

*Projektdaten*

| Standarddaten |  |
| --- | --- |
| Projektname: | Fluid-DM |
| Projektstart: | 22.06.2021 |
| Projektende: | 28.07.2021 |
| Projektlaufzeit: | 5 Wochen |
| Verantwortliche: | Marie Mustermann, Max Musterfrau |
| Förderprogramm | Deutsche Mustergesellschaft für Forschung |
| ... | ... |

*Der Übersichtlichkeit halber werden hier und im Folgenden die beispielhaften Eintragungen in den Datenmanagementplan nach folgendem Schema farblich gekennzeichnet: Attributname: Eintrag hierzu* 


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase1-5.png)


**Use-Case 1 - Fluidtechnische Simulation**

*Erwartete Forschungsdaten*

Angaben zu Forschungsdaten:     
Welche Daten werden erwartet? 

 -   Textdokumente 			        
     *  .pdf-Format 	
     *  .html-Format   
 -   selbstverfasster Text  		    
     * .docx-Format 
 -   Programmcode 			        
     * .py-Format
 -   Simulationsergebnisse 		    
     * .csv-Format
 -   Fotos des Versuchsstands	    
     * .png-Format
 -   Messergebnisse 			    
     * .lvm-Format 


 -   aufbereitete Messerg.		    
     * .csv-Format
 -   Validierungsergebnisse 	    
     * .csv-Format 	
     * .png-Format   
 -   Selbstverfasste VÖ			    
     * .docx-Format 	
     * .pdf-Format 


**Use-Case 1 - Fluidtechnische Simulation**

*Erwartete Forschungsdaten*

|erwartete Datentypen | Erwartete Dateiformate |
| --- | --- |
| Textdokumente | .pdf  .html |
| selbstverfasster Text | .docx |
| Programmcode | .m (matlab Skript) |
| Simulationsergebnisse | .csv |
| Fotos des Versuchsstands | .png |
| Messergebnisse | .lvm |
| Validierungsergebnisse | .csv .png |
| Selbstverfasste VÖ | .docx .pdf |


**Use-Case 1 - Fluidtechnische Simulation**

*Datenlebenszyklus*

Angaben zu Forschungsdaten:      
Welchen Umfang werden die Daten haben? (kB, MB, GB, TB,…?) 1-10 GB            
Wie werden die Daten...
 *  produziert? 
    - Recherche
    - Messung 
    - Simulation
 *  verarbeitet? 
    - Selbstgeschriebene Skripte
 *  ausgewertet? 
    - Selbstgeschriebene Skripte
 *  archiviert? 
    - Auf Instituts-Server


**Use-Case 1 - Fluidtechnische Simulation**

*Metadaten*
 
* Werden Metadaten erfasst? Wenn ja welche? 
  - Ja
  - Datums und Zeitangaben, Durchführende Person, ...
* Wie und wo werden Metadaten erfasst? 
  - Manuelle Eingabe und automatische Erfassung 
  - Auf Instituts-Server


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase1-6.png)
 

**Use-Case 1 - Fluidtechnische Simulation**

*Datenarchivierung*

Archivierung und Nachnutzung:   
* Wo und wie werden die Daten nach dem Projekt gespeichert?
  - Instituts-Server (Archiv), teilweise auf Zenodo veröffentlicht
* Welche Daten sollen wie geteilt werden?
  - Validierungsergebnisse 	in *.csv-Format und *.png-Format
  - Selbstverfasste VÖ		in *.docx-Format und *.pdf-Format
  - Veröffentlichung auf Zenodo


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase1-7.png)


**Use-Case 1 - Fluidtechnische Simulation**

*Datenarchivierung*

Archivierung und Nachnutzung:
* Wie werden (alle) Daten gesichert?
  - Doppelte Sicherung: 
  - auf Institutsserver  
  - auf CDs
* Wie lang werden die Daten gesichert? 
  - 10 Jahre            
*Zu Datenarchivierung gibt es ein separates NFDI4Ing Training mit Infos, Übungen und Toolempfehlungen*


**Use-Case 1 - Fluidtechnische Simulation**

*Overhead*

Overhead:    
* Datenschutz und rechtliche Aspekte 
  - DSGVO-relevant?  Nein 
  - Geheimhaltungserklärungen
* Kosten
  - Stellen, Gehälter und Dauer
  - Weitere Anschaffungen (z.B. Sachkosten)
* Projektplanung
  - Zeitrahmen
  - Meilensteine
* etc. 


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase1-8.png)
