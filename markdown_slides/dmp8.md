## 8. Use-Cases: Anwendungsbeispiele 

**Use-Case 2 - KI-based Decision Support Systems**

*Themenvorstellung: Mensch-Maschine-Interaktion in Entscheidungsprozessen*

* Kurzschreibung
  - Die Projektidee besteht darin, die Vorzüge von KI-Methoden mit denen menschlicher Urteilskraft in einen  Entscheidungsprozess zu bringen, der die arbeits-psychologische Belastung von Mitarbeitenden in den Arbeitsbereichen Service und Kundendienst reduziert. 
* Vorgehensweise
  - Anforderungs- und Bedarfsanalyse
  - Prototypische Erarbeitung KI-Anwendungen
  - Gestaltung der Mensch-KI-Schnittstelle
  - Entwicklung und Programmierung
  - Arbeitspsychologische Implikationen hinsichtlich Belastung und Beanspruchung
  - Transformationskonzept     


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase2-1.png)

[Bild: https://https://miro.com/app/dashboard/]


**Use-Case 2 - KI-based Decision Support Systems**

*Übersicht über die Arbeitspakete*

Beinhaltete Arbeitspakete:    
* AP-1:		Anforderungs- und Bedarfsanalyse 
* AP-2:		Prototypische Erarbeitung KI-Anwendungen 
* AP-3:		Gestaltung der Mensch-KI-Schnittstelle
* AP-4:		Entwicklung und Programmierung
* AP-5:		Arbeitspsychologische Implikationen hinsichtlich Belastung und Beanspruchung
* AP-6:		Transformationskonzept
* AP-7:		Projektmanagement und Ergebnistransfer     


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase2-2.png)


**Use-Case 2 - KI-based Decision Support Systems**

*Projektdaten*

Standarddaten:          
* Projektname sowie Projektstart und -ende:
  - KIDSS 08.10.2020 - 07.10.2023
* Verantwortliche/r: 
  - Max Mustermann
* Förderprogramm: 
  - Bundesministerium für Arbeit und Soziales (BMAS)
* Forschungspartner:     
  - RWTH Aachen University, FH MusterFH, Musterfirma GmbH, Mustergesellschaft mbH, Beispiel GmbH, Muster-Interessensgemeinschaft       
*Dies ist ein echter Use-Case, der aus Datenschutzgründen anonymisiert wurde*          


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase2-3.png)
 

**Use-Case 2 - KI-based Decision Support Systems**

*Erwartete Forschungsdaten*     

Angaben zu Forschungsdaten:
* Welche Daten werden erwartet?
  - Grafische Darstellungen (z.B. Prozessaufnahmen, Profile) (pptx.) oder als Liste (.xlsx)
  - Interview Transkriptionen (pdf.)
  - Screenshots Anwendungen Projektpartner (.jpeg, .png)
  - Programmcode
  - Umfragen (.pdf)
  - Nummerische Daten der Projektpartner (.csv)
  - Unterlagen Workshops und Whiteboard (Miro)   

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase2-4.png)


**Use-Case 2 - KI-based Decision Support Systems**

*Datenlebenszyklus*

Angaben zu Forschungsdaten:        
* Welchen Umfang werden die Daten haben? (kB, MB, GB, TB,...?)
  - Umfang einzelner Dateien: 0,1 - 10 MB
  - Voraussichtlicher Gesamtumfang der Daten im Projekt (inkl. aller Daten, die durch die Projektpartner zur Verfügung gestellt werden): 0,5 TB
* Wie werden die Daten...
  - produziert?
     - sammeln von Daten bei menschenzentrierten KI-Anwendungen
  - verarbeitet?
     - Informationen werden in Transformationskonzept eingeführt, welches relevante Daten berücksichtigt
  - archiviert? 
     - bisher nicht definiert
 

**Use-Case 2 - KI-based Decision Support Systems**

*Metadaten*

Metadaten:
* Werden Metadaten erfasst? Wenn ja welche?
  - bisher nicht definiert 
* Wie und wo werden Metadaten erfasst?
  - bisher nicht definiert              


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase2-5.png)


**Use-Case 2 - KI-based Decision Support Systems**

*Datenarchivierung*

Archivierung und Nachnutzung:
* Wo und wie werden die Daten nach dem Projekt gespeichert? 
  - Archivierungskonzept nach Projektende noch nicht definiert
* Welche Daten sollen wie geteilt werden?
  - Personenbezogene und unternehmensspezifische Daten werden anonymisiert; eine Veröffentlichung der Daten ist nicht vorgesehen
* Wie werden (alle) Daten gesichert?
  - Bei öffentlichen Forschungsergebnissen wird die Rückverfolgbarkeit zu personenbezogenen und unternehmensspezifischen Daten ausgeschlossen sein
* Wie lang werden die Daten gesichert? 
  - noch keine Angabe


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/dmp-datenmanagementplan/-/raw/master/media_files/Usecase2-6.png)


**Use-Case 2 - KI-based Decision Support Systems**

*Overhead*

Overhead:
* Datenschutz und rechtliche Aspekte
  - DSGVO-relevant?  Ja
  - frühestmögliche Anonymisierung personenbezogener Daten
* Kosten
  - Kostenplan.xlsx
* Projektplanung
  - 08.10.2020 - 07.10.2023
  - Projektplanung.pptx
* etc. 