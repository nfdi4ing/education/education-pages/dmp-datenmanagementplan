## 3. Motivation

* Wenn Forschende damit konfrontiert werden, einen Datenmanagementplan schreiben zu müssen, fehlt zum Einen oft die Erfahrung und zum Anderen wird das Erstellen eines Datenmanagementplans als zu aufwendig betrachtet
* Aus Erfahrung nimmt das Erstellen eines DMPs jedoch nicht viel Zeit in Anspruch, da es bereits einige Tools gibt, die das Anfertigen eines DMPs deutlich erleichtern
* Beispiele für solche Tools sind:
    * RDMO [Link]
    * [DMP-Tool vom MIT](https://libraries.mit.edu/data-management/plan/write/)


**Vorteile von DMPs**

* Die Daten sind gut beschieben und dokumentiert und daher leicht verständlich
* Bietet Kontinuität, wenn bestehende Projektmitglieder ausscheiden oder hinzukommen
* Dient als Bestandsaufnahme der Projektdaten, sodass unnötige Duplikate vermieden werden
* DMPs helfen, Fragen des Urheberrechts bereits in einem frühen Stadium zu berücksichtigen 
* Unterstützt bei der Bewertung von Optionen für die gemeinsame Nutzung von Daten und bei der Planung von Speichervorschriften
